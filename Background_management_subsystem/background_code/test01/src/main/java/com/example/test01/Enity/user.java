package com.example.test01.Enity;

import javax.persistence.*;

import xyz.erupt.annotation.sub_erupt.Power;
import xyz.erupt.annotation.sub_field.*;
import xyz.erupt.annotation.sub_field.sub_edit.*;
import org.hibernate.annotations.GenericGenerator;
import xyz.erupt.annotation.Erupt;
import xyz.erupt.annotation.EruptField;
import xyz.erupt.annotation.sub_field.Edit;
import xyz.erupt.annotation.sub_field.View;

@Erupt(
        name = "用户数据",primaryKeyCol = "user_id",
        power = @Power(add = true, delete = true,
                edit = true, query = true,
                importable = true, export = true)
)
@Table(name = "user")
@Entity
public class user {

    @Id
    @GeneratedValue(generator = "generator")
    @GenericGenerator(name = "generator", strategy = "native")
    @Column(name = "user_id")
    @EruptField(
            views = @View(
                    title = "user_id"
            ),
            edit = @Edit(
            title = "user_id",
            type = EditType.INPUT, search = @Search,
            inputType = @InputType
    )
    )
    private Integer user_id;

    @EruptField(
            views = @View(
                    title = "user_password"
            ),
            edit = @Edit(
                    title = "user_password",
                    type = EditType.INPUT, search = @Search(vague = true), notNull = true,
                    inputType = @InputType
            )
    )
    private String user_password;

    @EruptField(
            views = @View(
                    title = "user_name"
            ),
            edit = @Edit(
                    title = "user_name",
                    type = EditType.INPUT, search = @Search(vague = true), notNull = true,
                    inputType = @InputType
            )
    )
    private String user_name;

    @EruptField(
            views = @View(
                    title = "user_sex"
            ),
            edit = @Edit(
                    title = "user_sex",
                    type = EditType.NUMBER, search = @Search(vague = true),
                    numberType = @NumberType
            )
    )
    private Integer user_sex;

    @EruptField(
            views = @View(
                    title = "user_tel"
            ),
            edit = @Edit(
                    title = "user_tel",
                    type = EditType.INPUT, search = @Search(vague = true), notNull = true,
                    inputType = @InputType
            )
    )
    private String user_tel;

    @EruptField(
            views = @View(
                    title = "user_comment"
            ),
            edit = @Edit(
                    title = "user_comment",
                    type = EditType.INPUT, search = @Search(vague = true), notNull = true,
                    inputType = @InputType
            )
    )
    private Integer user_comment;

    @EruptField(
            views = @View(
                    title = "user_login"
            ),
            edit = @Edit(
                    title = "user_login",
                    type = EditType.INPUT, search = @Search(vague = true), notNull = true,
                    inputType = @InputType
            )
    )
    private Integer user_login;

}

/*
begin
declare user_uc int;
declare u_id int;
set user_uc=(select user_comment from user where user_id=new.user_id);
set u_id=(select user_id from user where user_id=new.user_id);
if user_uc>0 then update comment_check set user_comment=user_uc where user_id=u_id;
else update comment_check set user_comment=0 where user_id=u_id;
end if;
end
 */