package com.example.test01.Enity;

import xyz.erupt.annotation.Erupt;
import xyz.erupt.annotation.EruptField;
import xyz.erupt.annotation.sub_erupt.Power;
import xyz.erupt.annotation.sub_field.Edit;
import xyz.erupt.annotation.sub_field.EditType;
import xyz.erupt.annotation.sub_field.View;
import xyz.erupt.annotation.sub_field.sub_edit.InputType;
import xyz.erupt.annotation.sub_field.sub_edit.Search;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Erupt(
        name = "文物图片",primaryKeyCol = "img_id",
        power = @Power(add = true, delete = true,
                edit = true, query = true,
                importable = true, export = true)
)
@Table(name = "img_url_table")
@Entity
public class img_url_table {

    @Id
    @EruptField(
            views = @View(
                    title = "img_id"
            ),
            edit = @Edit(
                    title = "img_id",
                    type = EditType.INPUT, search = @Search,
                    inputType = @InputType
            )
    )
    private String img_id;

    @EruptField(
            views = @View(
                    title = "url"
            ),
            edit = @Edit(
                    title = "url",
                    type = EditType.INPUT, search = @Search(vague = true),
                    inputType = @InputType
            )
    )
    private String url;

}
