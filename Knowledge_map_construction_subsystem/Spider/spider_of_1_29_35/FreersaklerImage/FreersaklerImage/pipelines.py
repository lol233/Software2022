# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
from scrapy import Request


class FreersaklerimagePipeline(object):
    def process_item(self, item, spider):
        print(item['imagepath'])
        with open(item['imagepath'], 'wb') as f:
            f.write(item['image'])