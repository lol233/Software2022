# 爬虫部分

## 目标

+ 【1】弗利尔美术馆

https://www.freersackler.si.edu/collections/ 

https://asia.si.edu/collection-area/chinese-art/

+ 【29】丹佛美术博物馆

https://www.denverartmuseum.org/en

+ 【31】亚洲协会及其博物馆

http://museum.asiasociety.org/collection/explore/search/search:china--region:china--from-date:-3000--to-date:1950

+ 【33】鲁宾艺术博物馆

https://rubinmuseum.org/

+ 【35】 博尔州立大学艺术馆

https://www.bsu.edu/web/museumofart/collection 